# Google Pub/Sub client service example 

Since our part is about subscription this application is focused on it. 
However, it has a publisher also but only to provide some messages in Pub/Sub Google service.
(Here is a link to publisher guide - https://cloud.google.com/pubsub/docs/publisher)

### Subscriber

https://cloud.google.com/pubsub/docs/subscriber - Subscriber overview

#### Pull Subscriber

https://cloud.google.com/pubsub/docs/pull - pull subscriber guide

There are two APIs to create pull subscriber:
* [Current API](https://googleapis.dev/java/google-cloud-pubsub/latest/com/google/cloud/pubsub/v1/package-summary.html)
* [New Pub/Sub API (Beta)](https://googleapis.dev/java/google-cloud-pubsub/latest/com/google/cloud/pubsub/v1/stub/package-summary.html)

The second one (BetaAPI) is based on this - https://grpc.io/ 

There is an example of synchronous pull service that use New Pub/Sub API. [GcpSyncPullSubscriberService](https://bitbucket.org/tutorialstuff/pubsubservice/src/master/src/main/java/com/epam/service/GcpSyncPullSubscriberService.java)

Since it is Beta we will not use it. 

There is a [GcpAsyncPullSubscriberService](https://bitbucket.org/tutorialstuff/pubsubservice/src/master/src/main/java/com/epam/service/GcpAsyncPullSubscriberService.java) - asynchronous pull service

#### Default subscriber builder 
```
public Subscriber build() {
      if (systemExecutorProvider == null) {
        ThreadFactory threadFactory =
            new ThreadFactoryBuilder()
                .setDaemon(true)
                .setNameFormat("Subscriber-SE-" + SYSTEM_EXECUTOR_COUNTER.incrementAndGet() + "-%d")
                .build();
        int threadCount = Math.max(6, 2 * parallelPullCount);
        final ScheduledExecutorService executor =
            Executors.newScheduledThreadPool(threadCount, threadFactory);
        systemExecutorProvider =
            new ExecutorProvider() {
              @Override
              public boolean shouldAutoClose() {
                return true;
              }

              @Override
              public ScheduledExecutorService getExecutor() {
                return executor;
              }
            };
      }
      return new Subscriber(this);
    }
```

to provide some customization of executor we need to implement ExecutorProvider interface:

```
public Subscriber createAsyncSubscriber(MessageReceiver messageReceiver){
        return Subscriber.newBuilder(subscriptionName, messageReceiver)
                .setExecutorProvider(new ExecutorProvider() {
                    @Override
                    public boolean shouldAutoClose() {
                        return true;
                    }

                    @Override
                    public ScheduledExecutorService getExecutor() {
                        return Executors.newScheduledThreadPool(CORE_POOL_SIZE, new ThreadFactory() {
                            @Override
                            public Thread newThread(Runnable runnable) {
                                return new Thread(runnable, "Thread ### " + threadCounter.incrementAndGet());
                            }
                        });
                    }
                })
                .build();
    }
```


#### Push subscriber 

It could be a common spring boot application with the following controller part: 

##### - Controller Part

```
@PostMapping("/push")
    public ResponseEntity<String> receiveMessage(@RequestBody GcpPushNotification pushNotification) {
        byte[] decodedDataByteArray = Base64.getDecoder().decode(pushNotification.getMessage().getData());
        LOGGER.warn("Message received: " + new String(decodedDataByteArray));
        LOGGER.warn("Message ID: " + pushNotification.getMessage().getMessageId());
        LOGGER.warn("Message publish time: " + pushNotification.getMessage().getPublishTime());
        LOGGER.warn("Message subscription: " + pushNotification.getSubscription());
        return ResponseEntity.ok("OK");
    }
```

```
@Data
@NoArgsConstructor
public class GcpPushNotification {

    private PushMessage message;
    private String subscription;
}
```

```
@Data
@NoArgsConstructor
public class PushMessage {

    private String data;
    private Long messageId;
    private String publishTime;
}
```
##### - POM specific dependencies

```
 <dependencies>
    <dependency>
      <groupId>com.google.cloud</groupId>
      <artifactId>google-cloud-pubsub</artifactId>
    </dependency>
 </dependencies>

 <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.google.cloud</groupId>
        <artifactId>libraries-bom</artifactId>
        <version>5.4.0</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>
```
