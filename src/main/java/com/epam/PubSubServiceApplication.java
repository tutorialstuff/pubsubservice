package com.epam;

import com.epam.publisher.*;
import com.epam.service.GcpAsyncPullSubscriberService;
import com.epam.service.GcpPublisherService;
import com.epam.service.GcpSyncPullSubscriberService;
import com.epam.subscriber.AsyncMessageReceiver;
import com.epam.subscriber.GcpPullSubscriber;
import com.epam.subscriber.SyncSubscriberService;
import com.google.api.core.ApiFuture;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.cloud.pubsub.v1.stub.SubscriberStubSettings;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PubsubMessage;
import com.google.pubsub.v1.ReceivedMessage;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = "com.epam")
public class PubSubServiceApplication {


    public static void main(String[] args) {
        SpringApplication.run(PubSubServiceApplication.class, args);
    }

    @Value("${gcp.topic}")
    public String topic;

    @Value("${gcp.project}")
    public String project;

    @Value("${gcp.pull-subscriptionId}")
    public String pullSubscriptionId;

    @Autowired
    public MessageReceiver messageReceiver;

    @Bean
    public PublisherService getPublisherService(){
        return new GcpPublisherService(getMessagePublisher());
    }

    @Bean
    public MessagePublisher<PubsubMessage, ApiFuture<String>> getMessagePublisher(){
         return GcpMessagePublisher.createGcpMessagePublisher(project, topic);
    }

    @SneakyThrows
    @Bean(destroyMethod = "destroy")
    public GcpPullSubscriber getGcpPullSubscriber(){
        SubscriberStubSettings subscriberStubSettings =
                SubscriberStubSettings.newBuilder()
                        .setTransportChannelProvider(
                                SubscriberStubSettings.defaultGrpcTransportProviderBuilder()
                                        .setMaxInboundMessageSize(20 << 20) // 20MB
                                        .build())
                        .build();
        return GcpPullSubscriber.create(subscriberStubSettings);
    }

    @Bean
    public SyncSubscriberService<ReceivedMessage> getSyncSubscriberService(){
        String subscriptionName = ProjectSubscriptionName.format(project, pullSubscriptionId);
        return new GcpSyncPullSubscriberService(getGcpPullSubscriber(), subscriptionName);

    }

}
