package com.epam.controller;

import com.epam.publisher.PublisherService;
import com.epam.subscriber.AsyncSubscriberService;
import com.epam.subscriber.SyncSubscriberService;
import com.google.pubsub.v1.ReceivedMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PubSubController {

    private final PublisherService publisherProcessor;
    private final SyncSubscriberService<ReceivedMessage> syncSubscriberService;
    private final AsyncSubscriberService asyncSubscriberService;

    @Autowired
    public PubSubController(PublisherService publisherService, SyncSubscriberService<ReceivedMessage> syncSubscriberService, AsyncSubscriberService asyncSubscriberService) {
        this.publisherProcessor = publisherService;
        this.syncSubscriberService = syncSubscriberService;
        this.asyncSubscriberService = asyncSubscriberService;
    }


    @PostMapping("/pubsub/publish")
    public String publishMessage(@RequestBody String message) {
        return publisherProcessor.processMessage(message);
    }

    @GetMapping("/pubsub/pull")
    public List<String> pullMessages(@RequestParam(value = "limit", defaultValue = "10") int limit, @RequestParam(value = "acknowledge", defaultValue = "true") boolean acknowledge) {
        List<ReceivedMessage> messages = syncSubscriberService.getMessages(limit);

        if (acknowledge && !messages.isEmpty()) {
            syncSubscriberService.processAcknowledge(messages);
        }

        return messages
                .stream()
                .map(receivedMessage -> receivedMessage.getMessage().getData().toStringUtf8())
                .collect(Collectors.toList());
    }

    @GetMapping("/pubsub/pull/subscribe")
    public String subscribe() {
        asyncSubscriberService.subscribe();
        return "Subscribed";
    }

    @GetMapping("/pubsub/pull/unsubscribe")
    public String unsubscribe() {
        asyncSubscriberService.unsubscribe();
        return "Unsubscribed";
    }
}
