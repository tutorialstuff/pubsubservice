package com.epam.publisher;

import com.google.api.core.ApiFuture;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;
import lombok.SneakyThrows;

public class GcpMessagePublisher implements MessagePublisher<PubsubMessage, ApiFuture<String>> {

    @SneakyThrows
    public static GcpMessagePublisher createGcpMessagePublisher(String project, String topic){
        ProjectTopicName projectTopicName = ProjectTopicName.of(project, topic);
        Publisher gspPublisher = Publisher.newBuilder(projectTopicName).build();
        return new GcpMessagePublisher(gspPublisher);
    }

    private final Publisher gcpPublisher;

    private GcpMessagePublisher(Publisher gcpPublisher) {
        this.gcpPublisher = gcpPublisher;
    }

    @Override
    public ApiFuture<String> publish(PubsubMessage message) {
       return gcpPublisher.publish(message);
    }
}
