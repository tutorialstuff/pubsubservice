package com.epam.publisher;

public interface MessagePublisher<T, R> {

    R publish(T message);
}
