package com.epam.publisher;

public interface PublisherService {
    String processMessage(String message);
}
