package com.epam.service;

import com.epam.subscriber.AsyncSubscriberService;
import com.epam.util.InstanceCreator;
import com.google.api.core.ApiService;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;

@Service
public class GcpAsyncPullSubscriberService implements AsyncSubscriberService {

    private static final Logger LOGGER = LoggerFactory.getLogger("GcpAsyncPullSubscriberService");

    private final InstanceCreator instanceCreator;
    private final MessageReceiver messageReceiver;
    private Subscriber subscriber = null;

    public GcpAsyncPullSubscriberService(InstanceCreator instanceCreator, MessageReceiver messageReceiver) {
        this.instanceCreator = instanceCreator;
        this.messageReceiver = messageReceiver;
    }


    @Override
    public void subscribe() {
        subscriber = instanceCreator.createAsyncSubscriber(messageReceiver);
        subscriber.startAsync();
    }

    @Override
    public void unsubscribe() {
        if (subscriber != null) {
            stopSubscriber();
        }
    }

    private void stopSubscriber() {
        subscriber.stopAsync().awaitTerminated();
    }


    @SneakyThrows
    @PreDestroy
    private void destroy() {
        LOGGER.warn("PreDestroy");
        if (subscriber != null && (!ApiService.State.TERMINATED.equals(subscriber.state()))) {
            stopSubscriber();
        }
    }


}
