package com.epam.service;

import com.epam.publisher.MessagePublisher;
import com.epam.publisher.PublisherService;
import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutureCallback;
import com.google.api.core.ApiFutures;
import com.google.api.gax.rpc.ApiException;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class GcpPublisherService implements PublisherService {

    private final static Logger LOGGER = LoggerFactory.getLogger("GcpPublisherService");

    private final MessagePublisher<PubsubMessage, ApiFuture<String>> gcpMessagePublisher;

    public GcpPublisherService(MessagePublisher<PubsubMessage, ApiFuture<String>> publisher) {
        this.gcpMessagePublisher = publisher;
    }

    @SneakyThrows
    @Override
    public String processMessage(String stringMessage) {

        PubsubMessage pubSubMessage = preparePubSubMessage(stringMessage);
        ApiFuture<String> future = gcpMessagePublisher.publish(pubSubMessage);

        ApiFutures.addCallback(future, new ApiFutureCallback<String>() {
            @Override
            public void onFailure(Throwable throwable) {
                if (throwable instanceof ApiException) {
                    ApiException apiException = ((ApiException) throwable);
                    // details on the API exception
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Status code: ")
                            .append(apiException.getStatusCode().getCode())
                            .append("isRetryable: ")
                            .append(apiException.isRetryable());
                    LOGGER.error(stringBuilder.toString());

                }
                LOGGER.error("Error publishing message : {}", stringMessage);
            }

            @Override
            public void onSuccess(String messageId) {
                LOGGER.debug("messageId: {}", messageId);
            }
        }, MoreExecutors.directExecutor());

        return future.get();
    }

    private PubsubMessage preparePubSubMessage(String stringMessage) {
        ByteString data = ByteString.copyFromUtf8(stringMessage);
        return PubsubMessage
                .newBuilder()
                .setData(data)
                .build();
    }
}
