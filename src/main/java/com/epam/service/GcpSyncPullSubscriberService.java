package com.epam.service;

import com.epam.subscriber.GcpPullSubscriber;
import com.epam.subscriber.SyncSubscriberService;
import com.google.pubsub.v1.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.epam.util.SubscriberUtil.createAcknowledgeRequest;
import static com.epam.util.SubscriberUtil.createRequest;


public class GcpSyncPullSubscriberService implements SyncSubscriberService<ReceivedMessage> {

    private final GcpPullSubscriber subscriber;
    private final String subscriptionName;

    public GcpSyncPullSubscriberService(GcpPullSubscriber subscriber, String subscriptionName) {
        this.subscriber = subscriber;
        this.subscriptionName = subscriptionName;
    }


    @Override
    public List<ReceivedMessage> getMessages(int limit) {
        PullRequest request = createRequest(limit, subscriptionName);
        PullResponse pullResponse = subscriber.processPullRequest(request);
        return pullResponse.getReceivedMessagesList();
    }

    @Override
    public void processAcknowledge(List<ReceivedMessage> receivedMessages) {
        List<String> ackIds = receivedMessages.stream()
                .map(ReceivedMessage::getAckId)
                .collect(Collectors.toList());
        AcknowledgeRequest acknowledgeRequest = createAcknowledgeRequest(subscriptionName, ackIds);
        subscriber.processAcknowledgeRequest(acknowledgeRequest);
    }
}
