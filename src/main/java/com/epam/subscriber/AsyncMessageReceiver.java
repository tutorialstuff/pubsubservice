package com.epam.subscriber;

import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.pubsub.v1.PubsubMessage;
import org.springframework.stereotype.Component;

@Component
public class AsyncMessageReceiver implements MessageReceiver {


    @Override
    public void receiveMessage(PubsubMessage pubsubMessage, AckReplyConsumer ackReplyConsumer) {
        // handle incoming message, then ack/nack the received message
        System.out.println("Thread : " + Thread.currentThread().getName());
        System.out.println("Id : " + pubsubMessage.getMessageId());
        System.out.println("Data : " + pubsubMessage.getData().toStringUtf8());
        ackReplyConsumer.ack();
    }
}
