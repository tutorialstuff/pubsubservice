package com.epam.subscriber;

public interface AsyncSubscriberService {

    void subscribe();
    void unsubscribe();
}
