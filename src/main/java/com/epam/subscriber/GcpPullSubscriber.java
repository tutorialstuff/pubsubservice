package com.epam.subscriber;

import com.google.cloud.pubsub.v1.stub.GrpcSubscriberStub;
import com.google.cloud.pubsub.v1.stub.SubscriberStub;
import com.google.cloud.pubsub.v1.stub.SubscriberStubSettings;
import com.google.pubsub.v1.AcknowledgeRequest;
import com.google.pubsub.v1.PullRequest;
import com.google.pubsub.v1.PullResponse;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class GcpPullSubscriber {

    public static GcpPullSubscriber create(SubscriberStubSettings subscriberStubSettings) throws IOException {
        SubscriberStub subscriber = GrpcSubscriberStub.create(subscriberStubSettings);
        return new GcpPullSubscriber(subscriber);
    }

    private final SubscriberStub subscriber;

    private GcpPullSubscriber(SubscriberStub subscriber) {
        this.subscriber = subscriber;
    }

    public PullResponse processPullRequest(PullRequest request) {
        return subscriber.pullCallable().call(request);
    }

    public void processAcknowledgeRequest(AcknowledgeRequest acknowledgeRequest) {
        subscriber.acknowledgeCallable().call(acknowledgeRequest);
    }

    public void destroy() {
        if (subscriber != null) {
            try {
                subscriber.awaitTermination(10, TimeUnit.SECONDS);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
            subscriber.close();
        }
    }
}
