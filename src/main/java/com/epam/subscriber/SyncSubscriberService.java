package com.epam.subscriber;

import java.util.List;

public interface SyncSubscriberService<T> {

    List<T> getMessages(int limit);
    void processAcknowledge(List<T> receivedMessages);

}
