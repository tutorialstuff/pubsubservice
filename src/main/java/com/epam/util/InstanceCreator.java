package com.epam.util;

import com.google.api.gax.core.ExecutorProvider;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.pubsub.v1.ProjectSubscriptionName;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class InstanceCreator {

    public static final int CORE_POOL_SIZE = 10;
    private final String subscriptionName;
    private final AtomicInteger threadCounter;

    public InstanceCreator(@Value("${gcp.project}") String project, @Value("${gcp.pull-subscriptionId}") String pullSubscriptionId) {
        subscriptionName = ProjectSubscriptionName.format(project, pullSubscriptionId);
        threadCounter = new AtomicInteger(0);
    }

    public Subscriber createAsyncSubscriber(MessageReceiver messageReceiver){
        return Subscriber.newBuilder(subscriptionName, messageReceiver)
                .setExecutorProvider(new ExecutorProvider() {
                    @Override
                    public boolean shouldAutoClose() {
                        return true;
                    }

                    @Override
                    public ScheduledExecutorService getExecutor() {
                        return Executors.newScheduledThreadPool(CORE_POOL_SIZE, new ThreadFactory() {
                            @Override
                            public Thread newThread(Runnable runnable) {
                                return new Thread(runnable, "Thread ### " + threadCounter.incrementAndGet());
                            }
                        });
                    }
                })
                .build();
    }

}
