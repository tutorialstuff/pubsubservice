package com.epam.util;

import com.google.pubsub.v1.AcknowledgeRequest;
import com.google.pubsub.v1.PullRequest;

import java.util.List;

public class SubscriberUtil {

    public static PullRequest createRequest(int messagesLimit, String subscriptionName){
        return PullRequest.newBuilder()
                .setMaxMessages(messagesLimit)
                .setSubscription(subscriptionName)
                .build();
    }

    public static AcknowledgeRequest createAcknowledgeRequest (String subscriptionName, List<String> ackIds){
        return AcknowledgeRequest.newBuilder()
                .setSubscription(subscriptionName)
                .addAllAckIds(ackIds)
                .build();
    }
}
